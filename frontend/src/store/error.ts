import { writable } from 'svelte/store'
import type IError from '../interfaces/IError';


const init: IError = {
    msg: ""
}

const createError = () => {
    const { subscribe, update, set } = writable<IError>(init);

    return {
        subscribe,
        set: (value: string) => {
            console.log('new value: ', value);
            
            set({
                ...init,
                msg: value
            });
        },
        remove: () => {
            set(init)
        }
    }
}

export const error = createError() 