import { writable } from 'svelte/store'


import type ITodo from '../interfaces/ITodo'

const createTodo = () => {
    const { subscribe, update, set } = writable<Array<ITodo>>([]);

    return {
        subscribe,
        set: (value: Array<ITodo>) => {
            set(value);
        },
        add: (item: ITodo) => update((value) => {
            return [...value, item]
        }), 
        edit: (item: ITodo) => update((value) => {
            return value.map((v: ITodo) => {
                if (v.id === item.id) {
                    return item
                }
                return v
            })

        }),
        check: (id: string) => update((value) => {
            return value.map((v: ITodo) => {
                if (v.id === id) {
                    return {...v, isCompleted: !v.isCompleted}
                }
                return v
            })
        }),
        remove: (id: string) => update((value) => {
            return value.filter(f => f.id !== id); 
        })
    }
}

export const todo = createTodo() 