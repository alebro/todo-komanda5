const { validationResult } = require('express-validator')

const { v4: uuidv4 } = require('uuid');

const fs = require('fs')

const DB_NAME = 'db.json'

const todoSerivce = require('../service/todo.service')

class TodoController {
    getAll(req, res) {
        try {
            const data = todoSerivce.getData(DB_NAME)
            

            res.json({
                status: 'success',
                data: data
            })
        } catch (error) {
            res.json({
                status: 'error',
                message: error.message
            })
        }
    }

    addOne(req, res) {
        try {
            const errors = validationResult(req)

            if (!errors.isEmpty()) {
                res.status(400).send({
                    status: 'error',
                    errors: errors.array(),
                    message: 'Некорректные данные'
                })
                return;
            }

            const newObj = {
                id: uuidv4(),
                title: req.body.title,
                isCompleted: false
            }

            // Получение старых данных
            const oldData = todoSerivce.getData(DB_NAME)

            //Добавление новыйх данных
            todoSerivce.setData(DB_NAME, [
                ...oldData,
                newObj
            ])


            res.json({
                status: 'success',
                data: newObj
            })
        } catch (error) {
            res.json({
                status: 'error',
                message: error.message
            })
        }
    }

    changeOne(req, res) {
        try {
            const id = req.params.id;

            const data = todoSerivce.getData(DB_NAME)
            
            data.map(item => {
                if(item.id === id) {
                    item.title = req.body.title
                }
                return item;
            })

            todoSerivce.setData(DB_NAME, data)
            
            res.json({
                status: 'success',
                data: data
            })
        } catch (error) {
            res.json({
                status: 'error',
                message: error.message
            })
        }
    }

    checkOne(req, res) {
        try {
            const id = req.params.id;

            const data = todoSerivce.getData(DB_NAME)
            
            data.map(item => {
                if (item.id === id) {
                    item.isCompleted = !item.isCompleted
                }
                return item
            })

            todoSerivce.setData(DB_NAME, data)
            
            res.json({
                status: 'success',
                data: data
            })
        } catch (error) {
            res.json({
                status: 'error',
                message: error.message
            })
        }
    }

    deleteOne(req, res) {
        try {
            const id = req.params.id;

            const data = todoSerivce.getData(DB_NAME)
            
            const newData = data.filter(f => f.id !== id)

            todoSerivce.setData(DB_NAME, newData)

            res.json({
                status: 'success',
                data: newData
            })
        } catch (error) {
            res.json({
                status: 'error',
                message: error.message
            })
        }
    }

    deleteAll(req, res) {
        try {
            todoSerivce.setData(DB_NAME, [])

            res.json({
                status: 'success',
                data: []
            })
        } catch (error) {
            res.json({
                status: 'error',
                message: error.message
            })
        }
    }
}

module.exports = new TodoController()