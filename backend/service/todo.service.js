const fs = require('fs')

/*
[
    {
        "title": "aa",
        "isCompleted": false
    }
]
*/

class TodoService {
    getData(filename) {
        try {
            const data = fs.readFileSync(filename, 'utf-8')
            return JSON.parse(data)
        } catch (error) {
            fs.writeFile(filename, '', () => {})
            return []
        }
    }
    setData(filename, data) {
        fs.writeFile(filename, JSON.stringify(data), () => {})
    }
}

module.exports = new TodoService()