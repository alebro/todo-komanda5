const { Router } = require('express')
const { check } = require('express-validator')

const todoController = require('../controllers/todo.controller')

const router = Router()

router.get('/', todoController.getAll)

router.post('/', [
    check('title')
        .notEmpty().withMessage('Нельзя оставлять пустмы')
        .isString().withMessage('Должно быть строкой')
        .isLength({ max: 100 }).withMessage('Максиимальная длинна 100 символов')
], todoController.addOne)


router.patch('/:id', [
    check('title')
        .notEmpty().withMessage('Нельзя оставлять пустмы')
        .isString().withMessage('Должно быть строкой')
        .isLength({ max: 100 }).withMessage('Максиимальная длинна 100 символов')
], todoController.changeOne)

router.put('/:id', todoController.checkOne)

router.delete('/:id', todoController.deleteOne)
router.delete('/', todoController.deleteAll)

module.exports = router