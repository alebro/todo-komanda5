const express = require('express')
const cors = require('cors')


const PORT = 5000

const app = express()
app.use(express.json({ extended: true }))

app.use(cors({
    credentials: true,
    origin: ["http://46.39.135.17:8080/", "http://localhost:8080/", "http://46.39.135.17:8080", "http://localhost:8080", "http://127.0.0.1:8080", "http://127.0.0.1:8080/"]
}))

app.use('/api/todo', require('./routes/todo.routes'))

async function start() {
    try {

        app.listen(PORT, () => console.log(`App has been started on port ${PORT}...`))
    } catch (e) {
        console.log('Server Error', e.message)
        process.exit(1)
    }
}

start()